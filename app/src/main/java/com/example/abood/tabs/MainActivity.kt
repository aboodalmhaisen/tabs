package com.example.abood.tabs

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



val fragmentAdapter=MyPagerAdapter(supportFragmentManager)
        ViewPager.adapter=fragmentAdapter
        tabs.setupWithViewPager(ViewPager)
    }
}
