package com.example.abood.tabs

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.widget.Adapter


class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm){
    override fun getItem(p0: Int): Fragment {

return when(p0){

    0->{
        Fragment1()
    }
    1->{
        Fragment2()
    }
    else->{
        Fragment3()
    }

}
    }
//////////////////////////////////////////////////
    override fun getCount(): Int {

    return 3
    }
/////////////////////////////////////////////////////////////
    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){

            0->"Home"
            1->"Favorate"
            else->{
                return "Profile"
            }

        }
    }
}